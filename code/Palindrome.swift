/// Returns whether or not the given text is a palindrome.
func isPalindrome(_ text: String) -> Bool {

  // Write your code here.
  //
  // - Hint:
  //   Check the official documentation on Swift.String to see how to manipulate string
  //   indices (https://developer.apple.com/documentation/swift/string).

  fatalError("not implemented")

}

for string in ["kayak", "koala"] {
  let isStringAPalindrome = isPalindrome(string)
  print("'\(string)' is \(isStringAPalindrome ? "" : "not ")a palindrome")
}
